
public class AB2A3Temperaturtabelle {

	public static void main(String[] args) {
		double fa = -20	;	double ca = -28.8889;
		double fb = -10	;	double cb = -23.3333;
		double fc = 0	;	double cc = -17.7778;
		double fd = 20	;	double cd = -6.6667	;
		double fe = 30	;	double ce = +1.1111	;
		
		//Sinn der Formatierung, die ich gew�hlt habe ist es,
		//den Inhalt der Tabelle unabh�ngig von den Werten akkurat anzuzeigen,
		//daher habe ich mich dazu entschieden jeden Wert zu runden und auch den 
		//negativen Werten ein Vorzeichen aufzuzwingen,
		//was dazu f�hrt, dass auch glatte Zahlen, die letzten 2 Nachkommastellen
		//anzeigen. Man k�nnte dies f�r diese Werte �ndern, indem man bei diesen
		//statt "%+12.2f", "%+12s" nutzt.
		
		//head
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%11s","Celsius\n");
		//----------
		System.out.print("------------------------\n");
		//1st line
		System.out.printf("%+-12.2f",fa);
		System.out.print("|");
		System.out.printf("%+10.2f\n",ca);
		//2nd line
		System.out.printf("%+-12.2f",fb);
		System.out.print("|");
		System.out.printf("%+10.2f\n",cb);
		//3rd line
		System.out.printf("%+-12.2f",fc);
		System.out.print("|");
		System.out.printf("%+10.2f\n",cc);
		//4th line
		System.out.printf("%+-12.2f",fd);
		System.out.print("|");
		System.out.printf("%+10.2f\n",cd);
		//5th line
		System.out.printf("%+-12.2f",fe);
		System.out.print("|");
		System.out.printf("%+10.2f\n",ce);
		
	}

}
