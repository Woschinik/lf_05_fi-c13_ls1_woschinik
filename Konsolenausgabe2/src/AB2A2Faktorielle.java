
public class AB2A2Faktorielle {

	public static void main(String[] args) {
		//1st
		System.out.printf("%-5s","0!");
		System.out.printf("%-19s","=");
		System.out.print("=");
		System.out.printf("%4s","1\n");
		//2nd
		System.out.printf("%-5s","1!");
		System.out.printf("%-19s","= 1");
		System.out.print("=");
		System.out.printf("%4s","1\n");
		//3rd
		System.out.printf("%-5s","2!");
		System.out.printf("%-19s","= 1*2");
		System.out.print("=");
		System.out.printf("%4s","2\n");
		//4th
		System.out.printf("%-5s","3!");
		System.out.printf("%-19s","= 1*2*3");
		System.out.print("=");
		System.out.printf("%4s","6\n");
		//5th
		System.out.printf("%-5s","4!");
		System.out.printf("%-19s","= 1*2*3*4");
		System.out.print("=");
		System.out.printf("%4s","24\n");
		//6th
		System.out.printf("%-5s","5!");
		System.out.printf("%-19s","= 1*2*3*4*5");
		System.out.print("=");
		System.out.printf("%4s","120\n");
	}

}
