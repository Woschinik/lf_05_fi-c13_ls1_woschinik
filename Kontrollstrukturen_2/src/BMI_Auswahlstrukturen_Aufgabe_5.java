import java.util.Scanner;
public class BMI_Auswahlstrukturen_Aufgabe_5 {

	// BMI = Gewicht(Kg) / [Gr��e(cm)]^2
	// Klassifikation in m und w
	// m: Untergewicht< <20, 20<= Normalgewicht <=25, �bergewicht > 25
	// w: Untergewicht< <19, 19<= Normalgewicht <=24, �bergewicht > 24
	
	//Frage Gr��e, Gewicht, Geschlecht ab.
	//Gebe BMI raus
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double groesse;
		double gewicht;
		String geschlechtAus = "";
		double bmi = 0;
		
		System.out.println("Wie gro� sind Sie in Centimetern?");
		groesse = eingabe();
		System.out.println("Wie viel wiegen Sie in Kilogramm?");
		gewicht = eingabe();
		System.out.println("Welchem Geschlecht geh�ren Sie an?");
		geschlechtAus = geschlecht(geschlechtAus);
		bmi = bmicalc(groesse, gewicht, bmi);
		System.out.printf("Sie sind "+ groesse +"cm gro� und wiegen " + gewicht + "Kg.\nAls " + geschlechtAus + " haben Sie somit einen BMI von: ");
		System.out.printf("%.2f\n", bmi);
		auswertung(bmi, geschlechtAus);
	}

	public static int eingabe() {
		Scanner tastatur=new Scanner(System.in);
		int eingabe = tastatur.nextInt();
		return eingabe;
	}
	
	public static String geschlecht(String geschlechtAus) {
		Scanner tastatur=new Scanner(System.in);
		char eingabe = tastatur.next().charAt(0);
		if (eingabe == 'm') {
			geschlechtAus = "Mann";
		}else if (eingabe == 'w') {
			geschlechtAus = "Frau";
		}
		return geschlechtAus;
	}
	
	public static double bmicalc(double groesse, double gewicht, double bmi) {
		double groesse2 = groesse/100;
		groesse2= groesse2 * groesse2;
		bmi = gewicht/groesse2;
		return bmi;	
	}
	
	public static void auswertung(double bmi,String geschlechtAus ) {
		if (geschlechtAus == "Mann") {
			if  (bmi < 20) {
				System.out.println("Sie sind somit untergewichtig");
			} else if (bmi < 26) {
				System.out.println("Sie sind somit normalgewichtig");
			} else if (bmi > 25) {
				System.out.println("Sie sind somit �bergewichtig");
			}
		} else if (geschlechtAus == "Frau") {
			if  (bmi < 19) {
				System.out.println("Sie sind somit untergewichtig");
			} else if (bmi < 25) {
				System.out.println("Sie sind somit normalgewichtig");
			} else if (bmi > 24) {
				System.out.println("Sie sind somit �bergewichtig");
			}
		}
	}
}
