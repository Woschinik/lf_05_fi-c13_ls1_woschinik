import java.util.Scanner;
public class Taschenrechner_Fallunterscheidungen_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double zahl1 = eingabe();
		char operator = operator();
		double zahl2 = eingabe();
		double result = 0;
		boolean gultig = true;
		gultig = correct(gultig,operator);
		result = calculate(zahl1,zahl2,operator, result);
		if(gultig == true) {
		System.out.println(zahl1 + " "+ " "+ operator +" "+ zahl2 +" = "+ result);
		}
	}
	
	
	public static double eingabe() {
		Scanner tastatur=new Scanner(System.in);
		double eingabe = tastatur.nextDouble();
		
		return eingabe;
	}
	
	
	public static char operator() {
		Scanner tastatur=new Scanner(System.in);
		char operator = tastatur.next().charAt(0);
		return operator;
	}
	
	
	public static double calculate(double zahl1, double zahl2, char operator, double result) {
		if (operator == '+' || operator == '-' || operator == '*' || operator == '/') {
			if(operator == '+') {
				result = zahl1 + zahl2;
			}else if(operator == '-') {
				result = zahl1 - zahl2;
			}else if (operator == '*') {
				result = zahl1 * zahl2;
			}else if (operator == '/') {
				result = zahl1/zahl2;
			}
			
			
		}else {
			System.out.print("Eingabe ung�ltig!");
		}
		return result;
	}
	
	
	public static boolean correct(boolean gultig, char operator) {
		if (operator == '+' || operator == '-' || operator == '*' || operator == '/') {
			gultig = true;
		}else {
			gultig = false;
		}
		return gultig;	}
	
}
