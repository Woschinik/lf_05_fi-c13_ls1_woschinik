import java.util.Scanner;
public class Schaltjahr_Auswahlstrukturen_Aufgabe_8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Geben Sie ein Jahr ein, um zu testen,\nob es ich um ein Schaltjahr handelt:");
		Scanner tastatur=new Scanner(System.in);
		int eingabe = tastatur.nextInt();
		boolean rule1 = rule1m(eingabe);
		boolean rule2 = rule2m(eingabe,rule1);
		boolean rule4 = rule4m(eingabe,rule2);
		boolean schaltjahr = false;
		
		if (rule1 == true && rule4 == true) {
			schaltjahr = true;
		}
		else if (rule1 == true && rule2 == true) {
			schaltjahr = true;
		}
		if (schaltjahr == true) {
			System.out.println("Das Jahr "+ eingabe +" ist ein Schaltjahr!");
		}else {
			System.out.println("Das Jahr " + eingabe +" ist kein Schaltjahr");
		}
	//	rule3m(eingabe,rule2);
	//	rule4m(eingabe,rule2);
		
	//	System.out.print(rule1 +"\n"+rule2);
		
		
//		if (rule1 == true && rule2 == true && rule3 == true) {
//			System.out.print("Das Jahr "+eingabe+" ist ein Schaltjahr");
//		}
	}
	public static boolean rule1m(int eingabe) {
		boolean rule = false;
		if (eingabe %4 == 0) {
			rule = true;
		}
		return rule;
	}
	
	public static boolean rule2m(int eingabe, boolean rule1) {
		boolean rule2 = false;
		if (eingabe%100 == 0 && eingabe%400 != 0) {
			rule2 = false;
		}else if (eingabe%400 == 0) {
			rule2 = true;
		}else if(eingabe%100 != 0) {
			rule2 = true;
		}
		return rule2;
	}
	
	public static boolean rule4m(int eingabe,boolean rule4) {
		if(eingabe < 1582 ) {
			rule4 = true;
		} else rule4 = false;
		return rule4;
	}
	
}
