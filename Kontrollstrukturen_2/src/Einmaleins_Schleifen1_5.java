import java.util.Scanner;
public class Einmaleins_Schleifen1_5 {

	public static void main(String[] args) {
		int i;
		int n;
		for(i=1;i<11;i++) {
			for (n=1;n<11;n++) {
				System.out.printf("%4s", i*n);
				if (n==10) {
					System.out.println("");
				}else {
					System.out.print(",");
				}
			}
		}
	}

}
