

import java.util.Scanner;

public class Matrix_Schleifen2_8 {

	public static void main(String[] args) {
		int zahl1 = zahlenEingabe();
		int zahl2 = zahl1;
		ausgabe(zahl1, zahl2);
	}

	public static int zahlenEingabe() {
		boolean pruefWert = true;
		int min = 1;
		int max = 10;
		int eingabe = 0;
		while(pruefWert) {
			System.out.print("Bitte eine Zahl zwischen 2 und 9 eingeben: ");
			Scanner nutzerEingabe = new Scanner(System.in);
			eingabe = nutzerEingabe.nextInt();
			if(eingabe > min && eingabe < max) {
				pruefWert = false;
			}
			else {
				System.out.println("Sie haben eine falsche Eingabe get�tigt.");
			}
		}
		return eingabe;
	}
	
	public static void ausgabe(int zahl1, int zahl2) {
		int min = 1;
		int max = 10;
		int i = 0;
		for(int zeile = min; zeile <= max; zeile ++) {
			for(int spalte = min; spalte <= max; spalte ++) {
				i = (((zeile-1)*10)+(spalte-1));
				int stelleEins = i % 10;
				int stelleZwei = (i/10) % 10;
				int quersumme = stelleEins + stelleZwei;
				if(i == 0) {
					System.out.printf("%5d", i);
				}
				else if(stelleEins == zahl1 || stelleEins == zahl2 || stelleZwei == zahl1 || stelleZwei == zahl2 || i % zahl1 == 0 || i % zahl2 == 0 || quersumme == zahl1 || quersumme == zahl2) {
					System.out.printf("%5s", "*");
				}
				else {
					System.out.printf("%5d", i);	
				}
			}
			System.out.println();
		}
	}	
}
