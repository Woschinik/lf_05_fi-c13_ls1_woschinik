import java.util.Scanner;
public class Million_Schleifen2_6 {

	public static void main(String[] args) {
		ablauf();
	}
	public static int zieljahr(double zinsSatz, double einzahlung, int jahre) {
		double verm�gen = einzahlung*zinsSatz;
		jahre = 1;
		while(verm�gen < 1000000) {
			verm�gen = verm�gen*zinsSatz;
			System.out.println("Jahr"+jahre+": "+verm�gen);
			jahre++;
		}
		return jahre-1;
	}
	
	public static double eingabe() {
		Scanner tastatur=new Scanner(System.in);
		double eingabe = tastatur.nextDouble();
		
		return eingabe;
	}
	public static void repeat() {
		System.out.println("Wollen Sie eine weitere Berechnung durchf�hren?\ny -> Ja\nn -> Nein\n");
		Scanner tastatur=new Scanner(System.in);
		char eingabe = tastatur.next().charAt(0);
		if (eingabe == 'y' || eingabe == 'n') {
			if(eingabe =='y') {
				ablauf();
			}else if(eingabe =='n') {
				System.exit(0);
			}
			
		}else {
			System.out.print("Ung�ltige Eingabe. Das Programm wird beendet.");
			System.exit(0);
		}
		
		;
	}
	
	
	public static void ablauf() {
		int jahre;
		jahre = 0;
		double zinsSatz;
		double einzahlung;
		System.out.println("Geben sie den Startbetrag \nund den ZinsSatz in Prozent an:");
		einzahlung = eingabe();
		zinsSatz = 1+(eingabe()/100);
		jahre = zieljahr(zinsSatz,einzahlung,jahre);
		System.out.println("Nach " + jahre + " Jahren \n�berschreitet das angelegte Verm�gen 1.000.000 Geldeinheiten");
		repeat();
	}
}
