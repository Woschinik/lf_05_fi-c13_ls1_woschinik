import java.util.Scanner;

public class A33Fahrkartenautomat {
	//Methode: Bestellung erfassen
	public static double fahrkartenbestellungErfassen() {
		System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\r\n"
				+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n"
				+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n"
				+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		Scanner tastatur = new Scanner(System.in);
		double ticketPreis = tastatur.nextDouble();
		if(ticketPreis == 1.0) {
			ticketPreis = 2.90;
		} else if(ticketPreis == 2.0) {
			ticketPreis = 8.60;
		}else if (ticketPreis == 3.0) {
			ticketPreis = 23.50;
		}else {
			System.out.println("Ung�ltige Eingabe");
		}
		System.out.println("Der Ticketpreis betr�gt: "+ticketPreis +" Euro.");
		System.out.println("Anzahl der Tickets: ");
		byte ticketAnzahl = tastatur.nextByte();
		double zuZahlenderBetrag = ticketAnzahl * ticketPreis;
		return zuZahlenderBetrag;	
	}
	//Methode: Geldeinwurf
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingeworfeneM�nze;
		double eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
	    	   System.out.print( "Noch zu zahlen: ");
	    	   System.out.printf("%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.println(" Euro.");
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	           
	       }
		return eingezahlterGesamtbetrag;
		}
		
	//Methode: Fahrscheinausgabe
	public static void fahrscheinAusgabe() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	 //Methode: R�ckgeldberechnung und -Ausgabe
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rueckgabebetrag > 0.0)
	       {
	   	   System.out.print("Der R�ckgabebetrag in H�he von "); 
	   	   System.out.print( rueckgabebetrag);
	   	   System.out.println(" EURO");
	   	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           	{
	        	  System.out.println("2 EURO");
	        	  rueckgabebetrag -= 2.0;
	           }
	           while(rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
	        	  rueckgabebetrag -= 1.0;
	           }
	           while(rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
	        	  rueckgabebetrag -= 0.5;
	           }
	           while(rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	        	  rueckgabebetrag -= 0.2;
	           }
	           while(rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
	        	  rueckgabebetrag -= 0.1;
	           }
	           while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	        	  rueckgabebetrag -= 0.05;
	           }
	          // Workaround des Rundungsfehlers, der entsteht, wenn man bspw. 1 - 2,55 rechnet.
	           
	           if(rueckgabebetrag >= 0.001 && rueckgabebetrag < 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	        	  rueckgabebetrag -= 0.05;
	           }
	           
	       }
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                   "vor Fahrtantritt entwerten zu lassen!\n"+
                   "Wir w�nschen Ihnen eine gute Fahrt.\n\n");
		
	}
	public static void programmAblauf() {
		// Bestellung erfassen
	       double zuZahlenderBetrag = fahrkartenbestellungErfassen();  
	       // Geldeinwurf
	       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	     
	       // Fahrscheinausgabe
	       fahrscheinAusgabe();
	       // R�ckgeldberechnung und -ausgabe 
	       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	}
    public static void main(String[] args)
    {
    	boolean neustart = true;
    	while(neustart == true) {
        	programmAblauf();

    	}
    	
    }
}
