﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    //1&2) Notieren Sie die Variablen, Datentypen und Operationen der Variablen.
    //Typ: Scanner	Name: tastatur 					Operationen: Konsoleneingabe
    //Typ: float	Name:zuZahlenderBetrag			Operationen: Arithmetische- & Relationale Operationen, Konsoleneingabe
    //Typ: float	Name:eingezahlterGesamtbetrag	Operationen: Arithmetische- & Relationale Operationen
    //Typ: float	Name:eingeworfeneMünze			Operationen: Arithmetische Operationen
    //Typ: float	Name:rückgabebetrag				Operationen: Arithmetische Operationen
    //Typ: float	Name:restbetrag					Operationen: Arithmetische & Relationale Operationen
    //Typ: byte 	Name:ticketAnzahl				Operationen: Arithmetische Operationen
    //Typ: int		Name:i							Operationen: Arithmetische- & Relationale Operationen
       Scanner tastatur = new Scanner(System.in);
       double zuZahlenderBetrag;  
       double eingezahlterGesamtbetrag; 
       double eingeworfeneMünze;
       double rückgabebetrag;
       double restbetrag;
       byte ticketAnzahl;
       double ticketPreis;
       
       System.out.print("Der Ticketpreis beträgt (EURO): ");
       ticketPreis = tastatur.nextDouble();
       System.out.print("Anzahl der Tickets: ");
       ticketAnzahl = tastatur.nextByte();
       zuZahlenderBetrag = ticketPreis * ticketAnzahl;
       System.out.print("Zu zahlender Betrag (EURO): ");

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   restbetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
    	   System.out.print( "Noch zu zahlen: ");
    	   System.out.printf("%.2f", restbetrag);
    	   System.out.println(" Euro.");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }


    public static double liesDouble() {
    	Scanner myScanner = new Scanner(System.in);
    	double doubleWert = myScanner.nextDouble();
    	return doubleWert;
    }
}

// 5)
// Ich habe für den Ticketpreis den Datentyp Double gewählt, 
//da für die Berechnungen und Anzeigen Nachkommastellen berücksichtigt werden und es kompatiblitätsprobleme bei der Nutzung mit Float gab.
//Für die Anzahl der Tickets habe ich den Datentyp Byte gewählt, da es doch sehr unüblich ist mehr als 127 Fahrkarten auf einmal zu erwerben.

// 6) 
//Berechnen wir den Wert der Variable zuZahlenderBetrag, so multiplizieren wir die Werte der Variablen
//ticketAnzahl und ticketPreis. Das Ergebnis wird als Datentyp Double in der Variable gespeichert 
//und auf zwei Nachkommastellen begrenzt ausgegeben.