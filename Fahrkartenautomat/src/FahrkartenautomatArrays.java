import java.util.Scanner;

public class FahrkartenautomatArrays {
	//Methode: Bestellung erfassen
	public static double fahrkartenbestellungErfassen() {
		double[] ticketPreisA= {2.90,3.30,3.60,1.90,8.60,9.0,9.6,23.5,24.3,24.9};
		String[] ticketNameA= {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
		System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin �ber die Ticketnummer aus:\r\n");
		for(int i=0;i<ticketPreisA.length;i++) {
			System.out.print("["+(i+1)+"]"+"Ticketpreis: "+ticketPreisA[i]+" Euro f�r: "+ticketNameA[i]+"\n");
		}
				
		Scanner tastatur = new Scanner(System.in);
		int ticketNr= tastatur.nextInt();
		if (ticketNr < ticketPreisA.length || ticketNr > ticketPreisA.length) {
			System.out.println("Ung�ltige Eingabe!");
			 for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
			 System.out.print("\n");
			programmAblauf();
		}
		double ticketPreis= ticketPreisA[(ticketNr-1)];
		
		
		System.out.println("Der Ticketpreis betr�gt: "+ticketPreis +" Euro.");
		System.out.println("Anzahl der Tickets: ");
		byte ticketAnzahl = tastatur.nextByte();
		double zuZahlenderBetrag = ticketAnzahl * ticketPreis;
		return zuZahlenderBetrag;	
	}
	//Methode: Geldeinwurf
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingeworfeneM�nze;
		double eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
	    	   System.out.print( "Noch zu zahlen: ");
	    	   System.out.printf("%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.println(" Euro.");
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	           
	       }
		return eingezahlterGesamtbetrag;
		}
		
	//Methode: Fahrscheinausgabe
	public static void fahrscheinAusgabe() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	 //Methode: R�ckgeldberechnung und -Ausgabe
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rueckgabebetrag > 0.0)
	       {
	   	   System.out.print("Der R�ckgabebetrag in H�he von "); 
	   	   System.out.print( rueckgabebetrag);
	   	   System.out.println(" EURO");
	   	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           	{
	        	  System.out.println("2 EURO");
	        	  rueckgabebetrag -= 2.0;
	           }
	           while(rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
	        	  rueckgabebetrag -= 1.0;
	           }
	           while(rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
	        	  rueckgabebetrag -= 0.5;
	           }
	           while(rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	        	  rueckgabebetrag -= 0.2;
	           }
	           while(rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
	        	  rueckgabebetrag -= 0.1;
	           }
	           while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	        	  rueckgabebetrag -= 0.05;
	           }
	          // Workaround des Rundungsfehlers, der entsteht, wenn man bspw. 1 - 2,55 rechnet.
	           
	           if(rueckgabebetrag >= 0.001 && rueckgabebetrag < 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	        	  rueckgabebetrag -= 0.05;
	           }
	           
	       }
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                   "vor Fahrtantritt entwerten zu lassen!\n"+
                   "Wir w�nschen Ihnen eine gute Fahrt.\n\n");
		
	}
	public static void programmAblauf() {
		// Bestellung erfassen
	       double zuZahlenderBetrag = fahrkartenbestellungErfassen();  
	       // Geldeinwurf
	       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	     
	       // Fahrscheinausgabe
	       fahrscheinAusgabe();
	       // R�ckgeldberechnung und -ausgabe 
	       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	}
    public static void main(String[] args)
    {
    	boolean neustart = true;
    	while(neustart == true) {
        	programmAblauf();

    	}
    	
    }
}
