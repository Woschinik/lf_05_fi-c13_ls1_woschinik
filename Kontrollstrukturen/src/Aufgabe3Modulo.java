import java.util.Scanner;
public class Aufgabe3Modulo {

	public static void main(String[] args) {
		int i = 1;
		int n = 200;
		
		durch7(i,n);
		durch4(i,n);
		

	}
	public static void durch7(int i, int n) {
		System.out.println("durch 7 teilbar von 1 bis "+ n +":");
		for(i=1;i<=n;i++) {
			int a= i % 7;
			if(a == 0) {
			System.out.print(i + " | ");
			}
			}
		System.out.print("\n");
		return;
	}
	
	public static void durch4(int i, int n) {
		System.out.println("\ndurch 4, aber nicht durch 5 teilbar von 1 bis "+ n +":");
		for(i=1;i<=n;i++) {
			int a= i % 4;
			int b= i % 5;
			if(a == 0 && b != 0) {
			System.out.print(i + " | ");
			}
			}
		System.out.print("\n");
		return;
	}

}
