import java.util.Scanner;
public class SchleifenZaehlen {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		int i = 1;
		int n = tastatur.nextInt();
		boolean up = true;
		for (i = 1; i <= n; i++) {
			System.out.print(i);
			System.out.print(" | ");
			System.out.println((n-i));
		}
	}
}
