import java.util.Scanner;
public class Aufgabe2Summe {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		int i = 1;
		int n = tastatur.nextInt();
		int sumA = 0 ;
		a(i,n, sumA);
		b(i,n,sumA);
		c(i,n,sumA);
	}
	//Methode a)
	public static void a(int i, int n, int sumA) {
	System.out.println("Aufgabe a)");
		for(i=1;i<n;i++) {
			sumA += i;
			System.out.print(i + "+");		
		}
		if(i == n) {
			sumA += i;
			System.out.println(i + "=" + (sumA));
		}
		
	}
	
	//Methode b)
	public static void b(int i, int n, int sumA) {
		System.out.println("Aufgabe b)");
		for (i=1; i<2*n;i++) {
			sumA += i;
			System.out.print(i + "+");		
		}
		if(i == 2*n) {
			sumA += i;
			System.out.println(i + "=" + (sumA));
		}
		return;
	}
	//Methode c)
	public static void c(int i, int n, int sumA) {
		System.out.println("Aufgabe c)");
		for (i=1; i <2*n+1;i++) {
			sumA += i;
			System.out.print(i + "+");		
		}
		if(i == 2*n+1) {
			sumA += i;
			System.out.println(i + "=" + (sumA));
		}
		return;	
	}
}
