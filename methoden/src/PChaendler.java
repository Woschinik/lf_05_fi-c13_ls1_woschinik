import java.util.Scanner;

public class PChaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		System.out.println("was m�chten Sie bestellen?");
		String artikel = liesString();
		
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt();

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesDouble();

		// Verarbeiten
		double nettogesamtpreis = verarbNettoPreis(anzahl, preis);
		double bruttogesamtpreis = verarbBruttoPreis(nettogesamtpreis, mwst);

		// Ausgeben
		rechnungAusgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
}


// E
public static String liesString() {
	Scanner myScanner = new Scanner(System.in);
	String stringWert = myScanner.next();
	return stringWert;
}


public static int liesInt() {
	Scanner myScanner = new Scanner(System.in);
	int intWert = myScanner.nextInt();
	return intWert;
}
public static double liesDouble() {
	Scanner myScanner = new Scanner(System.in);
	double doubleWert = myScanner.nextDouble();
	return doubleWert;
}

// V
public static double verarbNettoPreis(int anzahl, double preis) {
	double nettoPreis = anzahl * preis;
	return nettoPreis;
}

public static double verarbBruttoPreis(double nettogesamtpreis, double mwst) {
	double bruttoPreis = nettogesamtpreis * (1 + mwst / 100);
	return bruttoPreis;
}



///A
public static void rechnungAusgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
	System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
	System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
}
}


